package com.solafidei.pixhawk.pixhawk.APIContainer.Entities;

/**
 * Created by Johann on 10/29/2016.
 */

public class UserLoginDTO {
    public String Email;
    public String Password;

    public UserLoginDTO(String password, String email) {
        Password = password;
        Email = email;
    }
}
