package com.solafidei.pixhawk.pixhawk.APIContainer.Services;
import android.content.Context;
import android.widget.Toast;

import com.loopj.android.http.*;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by Johann on 10/29/2016.
 */

public abstract class APIService{
    private static final String BASE_URL = "http://172.20.20.114:80/api/"; // Temporary replace with IP/Hostname of API
    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void GET(String path, String params){
        return;
    }
    public static void GETAll(String path, String params){
        return;
    }
    public static void POST(Context c, String url, String json, JsonHttpResponseHandler j){
        StringEntity SE = null;
        try{
            SE = new StringEntity(json);
        }
        catch (UnsupportedEncodingException s){
            Toast.makeText(c, "Login failed, invalid details.",
                    Toast.LENGTH_LONG).show();
        }
        client.post(c, getAbsoluteUrl(url), SE, "application/json", j);
    }
    public static void PUT(String path, String params){
        return;
    }
    public static void DELETE(String path, String params){
        return;
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}

/** Use this contents to create a response
    JsonHttpResponseHandler j = new JsonHttpResponseHandler(){
        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
            super.onSuccess(statusCode, headers, response);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            super.onFailure(statusCode, headers, throwable, errorResponse);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
            super.onFailure(statusCode, headers, throwable, errorResponse);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String responseString) {
            super.onSuccess(statusCode, headers, responseString);
        }
};
*/